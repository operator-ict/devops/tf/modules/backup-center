data "azurerm_resource_group" "rg" {
  name = var.resource_group_name
}

resource "azurerm_data_protection_backup_vault" "backup_vault" {
  name                = var.vault_name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = var.location
  datastore_type      = var.datastore_type
  redundancy          = var.redundancy
  retention_duration_in_days = var.vault_retention
  identity {
    type         = "SystemAssigned"
  }
}

resource "azurerm_role_assignment" "contributor_role" {
  scope                = data.azurerm_resource_group.rg.id
  role_definition_name = "Disk Snapshot Contributor"
  principal_id         = azurerm_data_protection_backup_vault.backup_vault.identity[0].principal_id
}

resource "azurerm_role_assignment" "reader_role" {
  for_each             = var.backup_instances
  scope                = each.value.disk_id
  role_definition_name = "Disk Backup Reader"
  principal_id         = azurerm_data_protection_backup_vault.backup_vault.identity[0].principal_id
}

resource "azurerm_data_protection_backup_policy_disk" "backup_policy" {
  name                            = "backup-policy"
  vault_id                        = azurerm_data_protection_backup_vault.backup_vault.id
  backup_repeating_time_intervals = var.time_interval
  default_retention_duration      = var.policy_retention
  time_zone                       = "C. Europe Standard Time"
}

resource "azurerm_data_protection_backup_instance_disk" "backup_disk" {
  for_each                     = var.backup_instances
  name                         = each.value.instance_name
  location                     = var.location
  vault_id                     = azurerm_data_protection_backup_vault.backup_vault.id
  disk_id                      = each.value.disk_id
  snapshot_resource_group_name = data.azurerm_resource_group.rg.name
  backup_policy_id             = azurerm_data_protection_backup_policy_disk.backup_policy.id
}
