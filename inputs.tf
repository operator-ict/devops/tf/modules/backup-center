variable "resource_group_name" {
  type    = string
  default = null
}

variable "name" {
  type    = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "location" {
  type    = string
}

variable "redundancy" {
  type    = string
  default = "ZoneRedundant"
}

variable "vault_retention" {
  type    = number
  default = 14
}

variable "policy_retention" {
  type    = string
  default = "P7D"
}

variable "datastore_type" {
  type    = string
  default = "VaultStore"
}

variable "vault_name" {
  type    = string
}

variable "time_interval" {
  type    = list(string)
}

variable "backup_instances" {
  type = map(object({
    instance_name   = string
    disk_id         = string
  }))
}
